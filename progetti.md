# Progetti d'esame

Pensate a progetti che privilegino l'hardware (sensori e attuatori, interfacciamento) rispetto al software: cose come app, siti web, altri sistemi "grossi" devono essere solo corollari (e sono /out of scope/ rispetto al corso). Mi interessa che vi "scontriate" col mondo fisico.
Altra cosa importante: l'uso di sistemi /closed-loop/ cioè con un minimo di feedback.


## Modalità

Gruppi sono possibili, ma limitatamente a due studenti (oltre si rischia dispersione delle competenze).

Ogni studente o gruppo *deve* dichiararsi su questa pagina (seguire il template), alla sezione "in itinere".

Template:

	Nome progetto

	Autori: nome1 cognome1, nome2 cognome2

	Descrizione: breve descrizione (hw/sw) del progetto

	Link a repo: creare un repository git (su un server tipo github o gitlab o altro pubblico) e linkarlo qui

	Licenza scelta: (per la scelta consultare ad es. https://www.gnu.org/licenses/licenses.html)
	Attenzione: deve essere una licenza LIBERA, cfr. anche https://www.gnu.org/licenses/license-list.en.html#GPLCompatibleLicenses
	
	Data *indicativa* di presentazione: basta il mese


## Idee (alcune invero balzane, ma si fa anche per giocare)

* Creare nuovi (o vecchi) strumenti di misura digitali, esempio una bilancia a "stadera" che si... autobilancia (sembra un gioco di parole)
* Sperimentare con "encoder ottici" vari (dischi di cartone su cui disegnare pattern da leggere mediante LED + fotosensore)
* Apparecchio di "voto" per pubblico di programmi televisivi
* Telecomando TV programmabile (nel senso di: segue un calendario di programmi su cui piazzare la TV, magari accendendo anche altri sistemi, tipo casse esterne etc.) via MQTT o altro
* Robottini vari che esplorano un ambiente, anche molto banali (tipo con "baffi" a microswitch per saggiare gli ostacoli)
* Piano che si autostabilizza (perpendicolare alla gravità) mediante accelerometri
* Strumento "musicale" (percussioni? o simili) comandato via MIDI/OSC
* Ventilatore che si adatta all'umidità della pelle
* Mappare una stanza usando il "time of flight" sensor di ST
* Monopattino elettrico con recupero energia in frenata
* Sensori ambientali non banali: combinare più sensori "banali" per misurare una proprietà di un ambiente. Es. localizzazione indoor di un device, contare persone in un ambiente, ...
* Inseguitore solare (me lo ha proposto uno di voi, segnalo qui per dare idee ulteriori)
* Integrazione di sensori in sistemi domotici, spiego: oggi esistono molti sistemi (anche liberi, cfr. openHAB e HomeAssistant) di gestione domotica, tali sistemi "parlano" già molti protocolli standard (MQTT, etc.) e sono capaci di interfacciarsi con sensori "standard". Un buon esercizio potrebbe essere quello di realizzare un sensore e renderlo "integrabile" in questi sistemi, cioè dotarlo della capacità di parlare facilmente con tali sistemi, in modo da non dover costringere gli utilizzatori a scrivere ulteriore software, ma solo scrivere un file di configurazione.
* Installazioni (anche interattive) "artistiche": animazioni d'acqua (https://www.youtube.com/watch?v=gusJeslMbLc), luce (https://www.youtube.com/watch?v=KmjP5VwuqBw dal minuto 35 circa, ma anche tutto), suono, etc.
* ...

In generale, tanto per partire e superare il "blocco del maker": un sensore, un attuatore, controllo feedback (closed loop).

## In itinere (progetti iniziati, ancora da presentare all'esame)



### SmartBlind

*Autore*: Luca Ghirotto

*Descrizione*: SmartBlind è un controller per tapparelle integrato con la piattafoma HAP (https://developer.apple.com/homekit/) di Apple per l'IoT. Lo scopo del progetto sarebbe il poter controllare il funzionamanto di tapparelle (o tende elettriche) dai dispositivi Apple direttamente dall'applicazione Home, cosi da non dover creare un'App ad-hoc.
Il controller riceverà le istruzioni via WiFi e azionerà un motore bifase per tirare su o giù le tapparelle e controllerà gli eventuali input manuali dell'utente mettendosi a valle degli interruttori a muro e quindi intecettando gli azionamenti degli stessi.

*Repository*: https://github.com/Ghir8/SmartBlind

*Hardware*:
* ESP 32
* 2 Relè da 220V
* 2 Step-down da 220V a 3V

*Licenza scelta*: GPLv3

*Data*: Fine settembre (da concordare)




### Smart Remote

*Nome Progetto*: Smart Remote

*Autore*: Alessandro Mascaretti

*Descrizione*: Il telecomando in questione è basato su ESP32. Grazie all'utilizzo di uno script di shell o di un assistente vocale per smartphone (ancora da valutare quale possa essere la scelta migliore tra le 2),
verranno mandati dei comandi MQTT al telecomando, il quale dopo averli opportunamente interpretati, li trasformera' in segnale a infrarossi per interagire con la TV. Il telecomando sara' inoltre dotato
di una fotoresistenza per il rilevamento della luminosita' ambientale. Qualore l'utente stesse vedendo la TV al buio, il telecomando in automatico mandera' un comando
MQTT in modo che eventuali lampade, collegate allo stesso server MQTT e poste dietro alla TV, verranno accese. Ai fini del progetto le lampade verranno simulate con uno o due led.

*Hardware* (provvisorio, potrebbe variare e/o aumentare in corso di sviluppo):
* ESP32
* IR Sender
* IR Receiver
* Led
* Fotoresistenza

Nota: il circuito verra' saldato su millefori e non lasciato volante su breadboard, in quanto verra' poi anche effettivamente utlizzato in casa.

*Link a repo* : https://gitlab.com/AleMasca/smart-remote (In Allestimento. Verra' modificato e riempito in fase di sviluppo)

*Licenza scelta* : GPLv3


### Citofono IOT

*Autori* : Alessandro Sala, Federico Burlon

*Descrizione* : Si tratta di un citofono che comunica con i residenti tramite il servizio cloud-based di messaggistica istantanea Telegram grazie all'utilizzo del modulo ESP8266. Sono previsti due modalità d'uso.
* ONLINE: permette ai residenti di registrarsi al citofono in pochi passi e quindi di ricevere le notifiche quando viene composto il loro interno. Infine, ricevuta una richiesta di apertura, gli utenti possono decidere se aprire o meno il cancello direttamente tramite chat Telegram.
* OFFLINE: il citofono si avvierà in modalità offline qualora non dovessere riuscire a connettersi al WiFi configurato. In questa modalità è comunque possibile aprire il cancello tramite l'immissione di un codice segreto. 

All'installazione viene utilizzata una modalità speciale che permette all'utente di scegliere tra le reti WiFi disponibili quella con cui verrà configurato il citofono da quel momento in poi. Una volta effettuata questa operazione, il citofono cercherà sempre di riconnettersi a tale WiFi anche in seguito allo spegnimento dello stesso. 

*Link a repo* : [](url)https://gitlab.com/alessandro14/progettosistemiembedded (almost done)

*Licenza scelta* : GPLv3

*Data* : metà Dicembre (da concordare)



## Presentati (si lasciano qui per archivio, non aggiungere proposte-progetti dopo questa riga)


### ChaseTheSun

Autore: Lorenzo Bini

Descrizione: ChaseTheSun è un piccolo robot con ruote motorizzate che cerca e raggiunge un luogo illuminato direttamente dal sole nelle sue vicinanze. Se rileva di essere nuovamente all'ombra, si rimette in moto per tornare al sole. Può anche usare i sensori di luminosità per rilevare oggetti in avvicinamento e "scappare" da essi. L'hardware è costituito da una base con ruote motorizzate, fotoresistenze inserite in alloggiamenti di cartoncino nero che fanno da sensori di luminosità, un piccolo pannello solare che rileva l'illuminazione diretta da luce solare (le fotoresistenze non funzionano bene per tale scopo), e il controller ESP32. Il software è un loop che controlla il pannello e le fotoresistenze per decidere se e dove spostarsi. La rilevazione dei luoghi illuminati è principalmente basata sul fatto che le superfici illuminate direttamente dal sole registrano sempre valori molto più alti di quelle all'ombra quando "osservate" tramite una fotoresistenza. Gli alloggiamenti in cartoncino nero servono a schermare una fotoresistenza da fonti di luce diverse dalla superficie del terreno, ad esempio il cielo o il paesaggio intorno; in pratica si dà al sensore un "campo visivo" ben limitato, nonostante la fotoresistenza di per sè non ne abbia uno (questo aspetto è già stato testato all'aperto). Il progetto è solo in fase iniziale, per ora.

Repository: https://gitlab.com/ellebi/chasethesun (in Inglese)

Licenza scelta: GPLv3





### Interactive LED matrix

*Autore*: Mirko Milovanovic

*Descrizione*: Creazione di una matrice LED 17x17 controllata da un ESP32.
La matrice è (semi)interattiva, potendo essere controllata in 2 modi: attraverso il protocollo Artnet o E131 è possibile inviare animazioni e giochi di luce da un controller
esterno in modo wireless; oppure, creando un sensore "touch" utilizzando 2 sensori ad ultrasuoni per gli assi X e Y è possibile interagire con un minimo di UI (esempio sveglia con informazioni meteo, etc).

Vorrei pensare un attimo su quali altri sensori potrei integrare o quali altre funzionalità creare dato che penso sia un attimo semplice come progetto (ergo sono ben accette idee su come migliorare il tutto) (atrent: le funzionalità che hai citato sono sufficienti per un progetto d'esame, se implementi sia controllo diretto che via rete. se proprio vuoi aggiungere qualcosa metti un accelerometro e sposta un puntoluce sulla matrice in funzione dell'inclinazione)

*Hardware* (provvisorio):

* ESP32
* 2x ultrasonic sensor HY-SRF05
* SK9822 RGB LED strip
* Trasformatore 5V, 100W

*Link a repo* : [https://gitlab.com/kobimex/interactive-led-matrix](https://gitlab.com/kobimex/interactive-led-matrix) (in allestimento)

*Licenza scelta* : GPLv3

*Data di presentazione*: 31 luglio '19



### Termostato Nest fai da te

Autore: Alessio Medda

Descrizione: Termostato domotico basato su ESP-32 in grado di regolare di interagire con la caldaia.
È controllabile sia attraverso i comandi manuali sia interfacciandosi con google assistant e fornire informazioni sulla temperatura nell'abitazione .
Viene implementato un PID per migliorare la regolazione della temperatura.

(commento atrent: il Nest "impara" la programmazione settimanale man mano che lo usi, vorresti provare a implementare una cosa analoga?)

Hardware:
    -DHT11
    -Display Oled AZOLED12864-1000
    -2 bottoni
    -1 potenziometro

Link a repo: https://github.com/alemk96/Termostato

Licenza scelta: GPL 3.0





### ArduinoMicroSynth 

Nome progetto: ArduinoMicroSynth

Autore: Alessandro Chiesi

Descrizione: Intendo realizzare un synth analogico pilotato via MIDI attraverso un Arduino Uno. Il sintetizzatore è costituito da due organi fondamentali:

- un oscillatore costruito con un integratore e un comparatore i quali forniscono rispettivamente onda quadra e triangolare.Inoltre è presente un input, ricevente una tensione in ingresso, la quale sceglierà la frequenza dell'oscillatore.
- un amplificatore costruito con un operazionale che regola l'ampiezza dell'onda generata dall'oscillatore. l'amplificatore riceve anch'esso una tensione d'ingresso per regolare il volume in uscita

Il software si occupa di gestire 3 funzionalità principali:

- gestione dei messaggi MIDI in ingresso
- erogazione della tensione per l'oscillatore ,attraverso il PWM generato da Arduino , secondo il messaggio MIDI ricevuto, opportunamente filtrato.
- erogazione della tensione per l'amplificatore, attraverso un output digitale PWM, filtrato opportunatamente, per ottenere una tensione continua.

La dinamica è regolabile secondo un ADSR implementato via software, attraverso quattro potenziometri:

* Attacco: tempo necessario per raggiungere il volume massimo
* Decadimento: tempo necessario per raggiungere il volume di Sustain
* Sustain: ampiezza a cui il volume rimarrà costante finchè il tasto rimane premuto, dopo il tempo di decadimento
* Rilascio: tempo in cui, una volta rilasciato il tasto, il volume arriverà a zero

Link a repo: https://github.com/Keasys/ArduinoMicroSynth

Licenza scelta: GPLv3
	




### Ardumeteo

*Autore*: Matteo Carlo Giavarini

*Descrizione*: Ardumeteo ha come obbiettivo la realizzazione di una piattaforma integrativa per un impianto di domotica.
L'idea è quella di avere una piattaforma in grado di rilevare valori ambientali indoor come temperatura, umidità, luminosità, pressione ed altitudine per poi condividerli tramite protocollo MQTT con la piattaforma Home Assistant.
Tramite l'interfaccia grafica di Home Assitant si potranno visualizzare i grafici relativi ai dati rilevati, sarà possibile scegliere se controllare le luci interne in maniera manulare tramite l'uso di un potenziometro collegato ad un Arduino, oppure in maniera automatica in base ai valori rilevati dal sensore di luminostà.
Inoltre sempre tramite interfaccia grafica sarà possibile impostare il periodo di lettura dei dati. La logica della gestione dei parametri è implementata su Home Assitant grazie all'utilizzo di trigger ed automazioni che invieranno su topic MQTT gli opportuni comandi.
Per comodità ho scelto di installare il broker MQTT su un PC Linux e non su un raspberry come è consueto fare. I vari sensori ambientali sono stati collegati ad un ESP32 connesso tramite Wifi mentre il potenziometro per il controllo manuale delle luci è stato collegato ad un clone di Arduino Uno connesso con ethernet shield,
mentre le luci interne sono simulate da led collegati ad entrambe le piattaforme.
Ho scelto di utilizzare due dispositivi per sottolineare la possibilità di avere più dispositvi che interagiscono fra di loro.

*Repository*: https://github.com/matteocarlogiavarini/Ardumeteo (Il progetto è completo ma potrei apportare dei cambiamenti)

*Licenza scelta*: GPLv3

(atrent: ok, ricorda che è più importante la parte embedded rispetto alla parte "home assistant"... caveat emptor)


### Smart home

*Autore*: Daniele Coccia

*Descrizione*: Sistema domotico che implementa le funzionalità di:

  - **Sensore temperatura e umidità**
    Tramite un sensore **DHT11** misuro temperatura e umidità ambientale.
    Quando viene rilevato un eccessivo livello di umidità un'automazione di home assistant invia un messaggio mqtt che aziona un relè che accenderà un deumidificatore.

  - **Controllo consumo energetico**
    Quando viene ricevuto tramite mqtt un livello di consumo energetico eccessivo dal misuratore viene fatto suonare un **buzzer** e vengono spente le luci per un quanto di tempo cercando così di ridurre i consumi.

  - **Luci con intensità variabile**
    una **fotoresistenza** determina la luminosità ambientale, l'intensità luminosa delle lampade è regolata in locale tramite lettura dei pin.
    Viene inviato ad homeassistant la quantità di luce ambientale presente nella stanza.

  - **Sistema di allarme**
    sensore di movimento che alla rilevazione di un movimento fa suonare un buzzer, invia messaggio di rilevato movimento ad homeassistant.
    Possibilità di attivare o disattivare il sistema di allarme da homeassistant tramite mqtt.

*Repository*: https://github.com/daniC97/smartHome

*Licenza scelta*: GPLv3



### Braccio raccoglitore

*Autori*: Luca Papparotto, Alessandro Varotto

*Descrizione* 

Il progetto prevede la realizzazione di un braccio meccanico in grado di raccogliere oggetti e spostarli in un cestino secondo due modalità:

- manuale: il braccio viene comandato attraverso l'utilizzo di un joystick; una volta posizionato sull'oggetto e premuto il tasto del joystick il braccio automaticamente preleverà l'oggetto e lo sposterà nella zona designata.

- automatico: il braccio meccanico attraverso il sensore ultrasonico cerca, nel range della sua presa, oggetti da poter raccogliere e buttare nel cestino.

*Hardware e materiale utilizzato:*

- Esp32
- Servo sg90 x5 
- Joystick x1
- Sensore ultrasonico x1
- Led x2
- Microswitch x1
- Interruttore x1
- Millefori x2

*Repository*: https://gitlab.com/lucapap97/esame-sistemi-embedded

*Licenza scelta:* GPLv3



### Termostatino32

*Autore*: Samuel Albani

*Descrizione*: Il progetto consiste in un termostato realizzato con il microcontrollore ESP32.
Viene fatto uso di un termistore NTC per monitorare la temperatura (calcolata con la formula riportata su https://it.wikipedia.org/wiki/Termistore#Equazione_con_parametro_B),
ed un potenziometro per regolare la temperatura desiderata tra 20 e 40 °C;
l'innalzamento della temperatura oltre la somma tra quella desiderata ed una soglia di 2 °C (per evitare continue accensioni e spegnimenti
dovuti a disturbi di lettura) provoca l'accensione della ventola,
la cui velocità è controllata in PWM attraverso un transistor, in base alla differenza tra la temperatura rilevata e quella desiderata;
quando la temperatura rilevata scende sotto quella desiderata, la ventola si ferma.
Lo stato del sistema è indicato, oltre che attraverso dati inviati su seriale, anche da un LED RGB regolato in PWM che si accende di blu
se la temperatura è sufficientemente bassa, mentre sfuma dal verde al rosso man mano che la temperatura rilevata sale oltre quella desiderata,
proporzionalmente alla velocità della ventola.
Inoltre, vengono inviati dati su temperatura attuale, temperatura desiderata e velocità della ventola percentuale ad un broker MQTT sotto il topic "Termostatino32",
attraverso la libreria PubSubClient, ed è possibile impostare la temperatura desiderata anche via MQTT tramite "Termostatino32/imposta <valore_double>" (sottinteso in °C);
per tornare invece al controllo manuale della temperatura desiderata tramite potenziometro basta inviare via MQTT "Termostatino32/imposta " (con payload vuota).

*Repository*: https://gitlab.com/samuel.albani/termostatino32

*Licenza scelta*: GNU GPLv3




### SimSim

Nome progetto: SimSim

Autore: Andrei Ciulpan

Descrizione:

   Sistema di controllo accessi basato su Arduino.
   Il sistema funziona con diverse modalità di riconoscimento tra cui abbiamo:

     - RFID
     - telecomando (con RF receiver)
     - keypad per poter accedere via password

   Il sistema manda i log di accessi tramite una richiesta HTTP ad un database server in locale
   (ho creato anche la parte di back-end, ma non sta sul repo) con un ESP-01
   Il sistema dispone inoltre di un display LCD 16x2

Repository: https://github.com/Jolsty/SimSim

Documentazione su https://github.com/Jolsty/SimSim/wiki * WORK IN PROGRESS *

Licenza scelta: CC BY-ND




### Salvaduino

Autori: D. Bellisario

Descrizione: obiettivo del progetto è la realizzazione di un salvadanaio controllato da Arduino.
Il salvadanaio:
    * accetta monete da 0.50, 1 e 2 EUR;
    * mostra importo e numero di monete su un display 16 x 2 LCD;
    * salva i dati su EEPROM per mantenere le informazioni anche in caso di spegnimento;
    * autorizza il prelievo solo in possesso di un token RFID;

(altre informazioni sul wiki [[https://github.com/DB375237/salvaduino/wiki]] dedicato)


Repository: https://github.com/DB375237/salvaduino

Licenza scelta: CC0 1.0 Universal.



### RadioArduino

Autori: Adriano Cofrancesco

Descrizione: RadioArduino è un progetto che ha lo scopo di emulare una Radio FM. In particolare questa radio può essere pilotata manualmente, con l'uso di un potenziometro per il cambio frequenza, oppure attraverso appositi messaggi via WiFi utilizzando il protocollo MQTT. Basandosi sullo schema publish/subscribe con MQTT RadioArduino può operare nella modalità  Subscriber, in tal caso può solo ricevere messaggi e rispondere al Publisher in maniera automatica a specifiche richieste, oppure come Publisher, in questo caso oltre alle funzionalità del subscriber può cambiare la frequenza a tutti i subscribers, verificare su quale frequenza sono sintonizzati e anche richiederne l'identificativo. Ogni subscriber apparterrà ad un gruppo, inizialmente di default, che potrà essere cambiato in autonomia così da ricevere messaggi da publisher diversi.

Hardware:
  * ESP8266 NodeMCU
  * LCD 16x2
  * 2x potenziometri 10K
  * TEA5767 FM Radio module

Link a repo: https://github.com/adrianocofrancesco/RadioArduino

Licenza scelta: GPLv3



### RemoteControlledGreenhouse

Autore: Marco De Nicolo

Descrizione: Ho realizzato una serra che può essere controllata da qualsiasi dispositivo, connesso alla stessa rete di questa, attraverso il protocollo OSC.
In particolare si può utilizzare un server per gestire, attraverso un'interfaccia grafica, una o più serre e renderle accessibili anche all'esterno della propria rete.
Per alimentare la serra ho utilizzato un power supply di un vecchio fisso, così da poter utilizzare tutti i diversi voltaggi che mi servivano.
La serra è automatizzata, quindi se l'igrometro segna un'umidità del terreno troppo bassa si apre l'elettrovalvola per innaffiare, se la temperatura o l'umidità è troppo alta si azionano le ventole e così via (i livelli minimi e massimi si possono settare).
La serra comunica con un server per settare l'ora e permettere all'utente di aggiungere innaffiature programmate.
La luce può essere automatica, quindi si accende con il buio, oppure manuale.
HW utilizzato:
  * 2 Ventole
  * elettrovalvola
  * led rosso (si può utilizzare una lampada adatta)
  * igrometro
  * sensore umidità e temperatura DHT11
  * sensore livello acqua
  * fotoresistenza
  * relè 4 canali
  * esp8266
  * power supply
  * breadboard, cavi jumper e altri componenti non elettronici

Link a repo: https://github.com/Maerk/RemoteControlledGreenhouse , https://github.com/Maerk/RemoteControlledGreenhouseServer

Licenza scelta: MPL 2.0







### SmartGarden

Autori: Alessandro Gigliotti, Giovanni Reni

Descrizione: in questo progetto, abbiamo realizzato un sistema di gestione intelligente di un piccolo orto o giardino. È presente un impianto d'irrigazione, basato sul controllo di ciò che accade e quindi di come si modifica, l'ambiente circostante. L'impianto entra infatti autonomamente in azione, quando si verificano determinate condizioni (come secchezza del terreno o assenza di pioggia), tenendo costantemente sotto controllo varie informazioni sull'aria, terra, meteo e sullo stato degli strumenti d'irrigazioni utilizzati. L'irrigazione può entrare in funzione, utilizzando una cisterna che raccoglie l'acqua piovana, oppure tramite un impianto idraulico. È altresì possibile attivare l'irrigazione anche manualmente da un utente tramite un pulsante fisico. Le informazioni rilevate tramite i sensori in giardino, vengono inviate e visualizzate su un display, attaccato ad un'altra board, posizionato in un luogo chiuso (ad esempio in casa). Affianco al display, ci saranno anche 4 led, che segnalano la presenza/assenza di pioggia, la presenza/assenza di sole, lo stato del serbatoio pieno o vuoto. Ci sono infine 3 pulsanti fisici, posizionati accanto al display, per azionare manualmente l'irrigazione, per accendere una lampada per illuminare e un tasto per arrestare entrambe queste attività. Inoltre il sistema sfrutta un'applicazione mobile, dalla quale sono consultabili tutte le informazioni lette dai sensori e lo stato della cisterna. Sono presenti inoltre 3 pulsanti virtuali, gemelli di quelli fisici, per azionare gli strumenti del progetto da un dispositivo. Infine l'app invia anche notifiche, relative agli eventi che accadono nel sistema.

Link a repo: https://github.com/GioReni/SmartGarden

Licenza scelta: GPLv3

### The Mouse

Autori: Simone Calcaterra, Matteo Negri

Descrizione: L'idea del progetto è quella di creare un mouse robotizzato, capace di percorrere tragitti prestabiliti secondo misure date.

per fare ciò utilizziamo la tecnologia del mouse per avere un riscontro sulla tratta percorsa in modo tale da compiere tragitti più o meno precisi.

Il software da noi utilizzato è una libreria, la PS/2 mouse presente in internet.
L'hardware da noi utilizzato è:
  * Un mouse PS\2
  * Una board Arduino pro Micro con integrato ATMEGA32U
  * Due motori DC da 3.3 V

Il nostro progetto focalizza molto l'attenzione sulla libreria e sul protocollo PS\2.

Link a repo: https://github.com/mnegri/TheMouse

Licenza scelta: GPLv3



### Macchina "intelligente"

Autori: Magni Andrea & Mercanti Davide

Descrizione: Il progetto che abbiamo deciso di realizzare prevede la costruzione di una "macchina intelligente". La macchina che costruiremo andrà a svolgere svolgere diverse funzioni di valutazione e risoluzione di problematiche.
  * La macchina dovrà riuscire a muoversi in una strada composta da due corsie rimanendo all'interno di una corsia grazie alla presenza di quattro sensori ad infrarossi posizionati nella parte frontale.
  * La presenza di un semaforo per gestire il traffico gestito da una board differente rispetto a quella della macchina. Entrambe le board avranno a disposizione un modulo radio per consentire la comunicazione.  
  * Nel caso in cui la macchina si stia avvicinando ad un semaforo valuti la sua condizione e regoli la velocità di conseguenza.
  * La presenza di possibili ostacoli durante il percorso, i quali verranno individuati da un modulo a ultrasuoni nella parte frontale della macchina.
  * Nel caso in cui la macchina trovi un ostacolo durante il percorso, i quali possono essere considerati fissi (una macchina in panne, lavori in corso) o mobili (dei pedoni che attraversano la strada) valuti il tipo di ostacolo e decida se continuare il percorso o effettuare una manovra di cambio corsia per poi continuare il percorso.

Link a repo: https://github.com/AndreaMagni/SistemiEmbedded

Licenza scelta: GPLv3





### SHARON-Macchina Self Tuning & Path Follower

Autori: Simone Scaravati, Noah Rosa, Stefano Radaelli

Descrizione: Sharon è una macchina controllabile tramite WiFi, grazie all'uso del protocollo OSC.
È in grado di bilanciare automaticamente la potenza dei suoi motori, in modo da poter andare dritta senza la necessità di andare a regolare manualmente la direzione da lei seguita(errore che capita spesso a causa dell'imprecisione dei motori).
Inoltre si interfaccia con un software (che sarà probabilmente disponibile per tutti i s.o.), scritto su Processing, che permetterà di disegnare graficamente un percorso, il quale sarà seguito dalla macchina il più fedelmente possibile.

Ai fini di testing, il progetto si appoggia all'app AndrOSC (disponibile gratuitamente su Play Store), per guidare manualmente la macchina e per farla anche muovere autonomamente grazie ad un sensore di distanza posto nella parte anteriore della carrozzeria.
Per poter utilizzare l'app facilmente mettiamo a disposizione un preset creato da noi, da collocare in un path specifico descritto nel readme.

Link a repo: https://github.com/simoneScaravati/Sharon-Path-Finder

Licenza scelta: GPLv3

### ESP8266 print server

Autore: Gianluca Nitti

Descrizione: print server basato su ESP8266 per aggiungere connettività di rete a stampanti con porta parallela o seriale. Le stampanti con porta parallela possono essere collegate direttamente (occupando 10 GPIO) oppure tramite shift register (usando "solo" 5 pin di I/O). Sono supportati i protocolli di rete [[http://lprng.sourceforge.net/LPRng-Reference-Multipart/appsocket.htm|AppSocket]] ed [[https://en.wikipedia.org/wiki/Internet_Printing_Protocol|IPP]]. Presente anche una coda di stampa sulla memoria flash a bordo dell'ESP8266 per poter accettare altri job anche quando la stampante è occupata, ma attualmente funziona solo se il job di stampa in attesa rientra completamente nella memoria (quando la stampante si libera, prima finisce di ricevere il nuovo job e poi inizia a stamparlo; migliorabile perchè potrebbe iniziare non appena la stampante è libera, facendo quindi spazio sulla flash).

Link a repo: https://github.com/gianluca-nitti/printserver-esp8266/ (il branch develop viene aggiornato più spesso, facendo il merge in master solo quando è completato lo sviluppo di una funzionalità significativa)

Licenza scelta: GPLv3

### DAMS_01 Sintetizzatore digitale

Autore: Marco Colussi

Descrizione: Il DAMS_01 è un sintetizzatore digitale sviluppato in PureData che utilizza dei sensori ambientali per creare una sintesi diversa e unica in ogni luogo in cui viene usato. Dal lato hw vediamo l'impiego di un raspberry PI3 per far girare il software di PureData, un Arduino UNO per la gestione degli input e la comunicazione con il sintetizzatore via firmata_extended, codice Arduino che modifica il già esistente standardFirmata per permettere di falsare letture e la quantità di pin analogici presenti sulla board.
I sensori utilizzati sono:
  - RGB Led;
  - ADXL 345;
  - Touch sensor;
  - LDR;
  - DHT11;
  - Analog Thumb Joystic;
  - Multiplexer CD4067BE;
  - 5x potenziometri 10kΩ;
  - 5x bottoni;


Link a repo: https://github.com/warpcut/DAMS_01

Licenza scelta: GPLv3

### SolarTracker

Autore: Mirco Gnuva

Descrizione: Sistema in grado di modificare l'inclinazione di un pannello fotovoltaico su due assi tramite servo motori in base alla posizione relativa del sole. La posizione relativa del sole viene determinata utilizzando 4 celle fotovoltaiche che si muovono insieme al pannello principale. Il sistema è inoltre in grado di caricare batterie di tipo 18650 Li-ion e di determinarne lo stato di carica. Il tutto viene controllato tramite un ESP32 (montato su Lolin 32). La logica di controllo è realizzata in Micropython (porting di Python).
Componenti hardware:
  - 2 display oled I2C;
  - Step-down LM2596;
  - Step-up (modello non disponibile);
  - 4 celle fotovoltaiche policristalline (modello non disponibile);
  - Pannello fotovoltaico 5.2W 12V policristallino (modello non disponibile);
  - Bilanciatore di carica/scarica TP4056;
  - Relè 3.3V (modello non disponibile);
  - 2 Servo motori MG90S;
  - Resistori (modello non disponibile);
  - Lolin 32;
In caso di necessità i link di ogni componente sono disponibili su repo(Creazione del README in corso) o tramite mail

Link a repo: https://github.com/Wasp971/SolarTracker

Licenza scelta: GPLv3

### RaspberryCreate

*Autore*: Davide D'Ascenzo, Andrea Scipioni

*Descrizione*: RaspberryCreate è una panoramica dei dispositivi Raspberry in ambiente embedded, con particolare attenzione all'utilizzo della piattaforma Arduino Create.

*Repository*: https://github.com/Kidara/RaspberryCreate

*Licenza scelta*: GPLv3


### Libramentum

*Autore*: Edoardo Ferrari

*Descrizione*: Libramentum è un telecomando al quale è stato collegato un piano movente. Ogni volta che si proverà a roteare il telecomando a destra
o a sinistra rispetto all'asse dell'hardware, il piano verrà stabilizzato e resterà sempre parallelo al terreno. Dati informativi riguardanti la stabilità
del piano verranno inviati tramite connessione MQTT ad un server hostato su https://www.cloudmqtt.com/

*Repository*: https://github.com/normaloide/Libramentum (in aggiornamento)

*Licenza scelta*: GPLv3



### F1 Timer

*Autore*: Mauro Mastrapasqua

*Descrizione*: F1-Timer è un sistema di cronometraggio wireless in grado di misurare i tempi di percorrenza di piu' macchine su un circuito di gara. E' basato sul probing wifi da parte delle macchine verso un'antenna comune presente nel circuito. Un server si occupa di elaborare questi dati e di stendere una classifica delle macchine in gara, oltre che a gestire altri componenti/servizi nel circuito: notifica tramite display della macchina che ha fatto il giro piu' veloce, gestione delle luci di gara (semafori di partenza, luci per segnalare giro veloce ecc...) e infine il sistema ridondante di cronometraggio basato su fotocellula. Il server quindi si occupera' di confrontare i dati acquisiti dai sensori e quelli acquisiti dalle macchine, inviati via wifi. Tutti i dati raccolti vengono messi a disposizione tramite un sito web.

*Repository*: https://github.com/mmastrapasqua/f1timer

*Licenza scelta*: 0BSD

### NOTA BENE: NON AGGIUNGERE I PROGETTI QUI IN FONDO, METTERLI NELLA SEZIONE "IN ITINERE"!!!
