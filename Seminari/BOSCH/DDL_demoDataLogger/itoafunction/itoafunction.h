/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  Created on: 14.01.2015
 *      Author: axel.kuntz
 */

#ifndef XDK_DATALOGGER_ITOAFUNCTION_ITOAFUNCTION_H_
#define XDK_DATALOGGER_ITOAFUNCTION_ITOAFUNCTION_H_

/** @brief
 * 		The function used by itoa
 *
 *  @param[in] char s[]
 * 		Char whic.
 */
void reverse(char s[]);

/** @brief
 * 		The function converts an integer to an char
 *
 *  @param[in] int n
 * 		Integer to convert
 *
 * 	@param[in] char s
 * 		Char gets converted integervalue
 */
void itoa(int n, char s[]);


#endif /* XDK_DATALOGGER_ITOAFUNCTION_ITOAFUNCTION_H_ */
