/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 * Demo application of printing BMA280 Accelerometer data on serialport(USB virtual comport)
 * every one second, initiated by autoreloaded timer(freertos)
 * 
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XdkSensorHandle.h"
#include "XDK_Datalogger_ih.h"
#include "XDK_Datalogger_ch.h"
#include "BMA_280_ih.h"
#include "BMA_280_ch.h"

/* system header files */
#include <stdio.h>
#include <BCDS_Basics.h>

/* additional interface header files */
#include "BSP_BoardType.h"
#include "BCDS_BSP_LED.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Accelerometer.h"
#include "BCDS_Retcode.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
Accelerometer_XyzData_T getAccelDataRaw = { INT32_C(0), INT32_C(0), INT32_C(0) };
Accelerometer_XyzData_T getAccelDataUnit =
        { INT32_C(0), INT32_C(0), INT32_C(0) };
Accelerometer_Range_T bma280range = ACCELEROMETER_RANGE_OUT_OF_BOUND;
Accelerometer_Bandwidth_T bma280bw = ACCELEROMETER_BANDWIDTH_OUT_OF_RANGE;

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* API documentation is in the interface header LSD_lightSensorDemo_ih.h*/

/** The function to get  the accel data
 * @brief Gets the data from BMA280 Accel
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void bma280_getSensorValues(void *pvParameters)
{
    (void) pvParameters;
    Retcode_T advancedApiRetValue = (Retcode_T) RETCODE_FAILURE;

    advancedApiRetValue = Accelerometer_readXyzLsbValue(
            xdkAccelerometers_BMA280_Handle, &getAccelDataRaw);
    if (RETCODE_OK != advancedApiRetValue)
    {
        printf("accelerometerReadXyzLsbValue FAILED\n\r");
    }
    advancedApiRetValue = Accelerometer_readXyzGValue(
            xdkAccelerometers_BMA280_Handle, &getAccelDataUnit);
    if (RETCODE_OK != advancedApiRetValue)
    {
        printf("accelerometerReadXyzGValue FAILED\n\r");
    }
}

/* global functions ********************************************************* */

/**
 * @brief The function initializes BMA(accelerometer) and set the sensor parameter from logger.ini
 */
extern void bma_280_init(void)
{
    /* Return value for Accel Sensor */
    Retcode_T accelReturnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnVal = RETCODE_OK;
    /*initialize accel*/
    accelReturnValue = Accelerometer_init(xdkAccelerometers_BMA280_Handle);
    if (RETCODE_OK == accelReturnValue)
    {
        printf("BMA280 initialization succeed\n\r");
        if (config.bma280_range == 2)
        {
            bma280range = ACCELEROMETER_BMA280_RANGE_2G;
        }
        else if (config.bma280_range == 4)
        {
            bma280range = ACCELEROMETER_BMA280_RANGE_4G;
        }
        else if (config.bma280_range == 8)
        {
            bma280range = ACCELEROMETER_BMA280_RANGE_8G;
        }
        else if (config.bma280_range == 16)
        {
            bma280range = ACCELEROMETER_BMA280_RANGE_16G;
        }
        accelReturnValue = Accelerometer_setRange(
                xdkAccelerometers_BMA280_Handle, bma280range);
        if ((RETCODE_OK != accelReturnValue)
                || ACCELEROMETER_RANGE_OUT_OF_BOUND == bma280range)
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed");

            }
        }
        /* check the bandwith value from INi-File*/
        if (strcmp(bma280_bw, "7.81") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_7_81HZ;
        }
        else if (strcmp(bma280_bw, "15.63") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_15_63HZ;
        }
        else if (strcmp(bma280_bw, "31.25") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_31_25HZ;
        }
        else if (strcmp(bma280_bw, "62.5") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_62_50HZ;
        }
        if (strcmp(bma280_bw, "125") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_125HZ;
        }
        if (strcmp(bma280_bw, "250") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_250HZ;
        }
        if (strcmp(bma280_bw, "500") == 0)
        {
            bma280bw = ACCELEROMETER_BMA280_BANDWIDTH_500HZ;
        }
        accelReturnValue = Accelerometer_setBandwidth(
                xdkAccelerometers_BMA280_Handle, bma280bw);

        if ((RETCODE_OK != accelReturnValue)
                || ACCELEROMETER_BANDWIDTH_OUT_OF_RANGE == bma280bw)
        {
            returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
            if (RETCODE_OK != returnVal)
            {
                printf("Turning on of RED LED failed");

            }
        }
    }
    else
    {
        printf("BMA280 initialization failed\n\r");
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }

    }
}

/**
 *  @brief API to Deinitialize the PAD module
 */
extern void bma_280_deInit(void)
{
    Retcode_T returnValue = RETCODE_FAILURE;
    returnValue = Accelerometer_deInit(xdkAccelerometers_BMA280_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("Accelerometer Deinit Success\n\r");
    }
    else
    {
        printf("Accelerometer Deinit Failed\n\r");
    }

}

/** ************************************************************************* */
