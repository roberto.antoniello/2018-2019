/* (2019 modified by atrent)

 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int potpin = 0;  // analog pin used to connect the potentiometer
int rawval;    // variable to read the value from the analog pin

void setup() {
    myservo.attach(2);  // attaches the servo on pin 9 to the servo object
    Serial.begin(115200);
}

void loop() {
    rawval = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
    int angle = map(rawval, 0, 1023, 0, 160);     // scale it to use it with the servo (value between 0 and 180)

    Serial.print(rawval);
    Serial.print(",");
    Serial.println(angle);

    myservo.write(angle);                  // sets the servo position according to the scaled value
    delay(20);                           // waits for the servo to get there
}
