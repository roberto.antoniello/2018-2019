import machine
import time


# Implementazione della funziona map di Arduino
def map_num(input, old_min, old_max, new_min, new_max):
	return (input - old_min) * (new_max - new_min) / (old_max - old_min) + new_min


# Calcolo peso rosso
def get_red(wavelength):
	if 400 <= wavelength < 410:
		delta = (wavelength-400)/10
		r = (0.33*delta)-(0.2*delta*delta)
	elif 410 <= wavelength < 475:
		delta = (wavelength-410)/65
		r = 0.14 - (0.13*delta*delta)
	elif 545 <= wavelength < 595:
		delta = (wavelength-545)/50
		r = (1.98*delta)-(delta*delta)
	elif 595 <= wavelength < 650:
		delta = (wavelength-595)/55
		r = 0.98+(0.06*delta)-(0.4*delta*delta)
	elif 650 <= wavelength < 700:
		delta = (wavelength-650)/50
		r = 0.65-(0.84*delta)+(0.2*delta*delta)
	else:
		r = 0
	return r


# Calcolo peso verde
def get_green(wavelength):
	if 415 <= wavelength < 475:
		delta = (wavelength-415)/60
		g = 0.8*delta*delta
	elif 475 <= wavelength < 590:
		delta = (wavelength-475)/115
		g = 0.8+(0.76*delta)-(0.8*delta*delta)
	elif 585 <= wavelength < 639:
		delta = (wavelength-585)/54
		g = 0.84-(0.84*delta)
	else:
		g = 0
	return g


# Calcolo peso blu
def get_blue(wavelength):
	if 400 <= wavelength < 475:
		delta = (wavelength-400)/75
		b = (2.2*delta)-(1.5*delta*delta)
	elif 475 <= wavelength < 560:
		delta = (wavelength-475)/85
		b = 0.7-delta+(0.3*delta*delta)
	else:
		b = 0
	return b


# Calcolo terna RGB
def to_rgb(value, max_pwm):
	wavelength = map_num(value, 0, 1023, 400, 700)
	print("Wavelength:", wavelength)
	r = get_red(wavelength)
	g = get_green(wavelength)
	b = get_blue(wavelength)
	# Mapping peso -> duty cycle
	return map_num(r, 0, 1, 0, max_pwm), map_num(g, 0, 1, 0, max_pwm), map_num(b, 0, 1, 0, max_pwm)


def main():
	adc_pin = 36  # Pin ADC (collegato al trimmer)
	blue_pin = 12  # Pin collegato all'anodo del blu
	green_pin = 14  # Pin collegato all'anodo del verde
	red_pin = 27  # Pin collegato all'anodo del rosso
	trimmer = machine.ADC(machine.Pin(adc_pin))  # Creazione oggetto ADC collegato al pic scelto
	trimmer.atten(trimmer.ATTN_11DB)  # Configurazione range 0-3.6V
	trimmer.width(machine.ADC.WIDTH_10BIT)  # Configurazione lettura su 10b
	b_pin = machine.PWM(machine.Pin(blue_pin), freq=980)
	g_pin = machine.PWM(machine.Pin(green_pin), freq=980)
	r_pin = machine.PWM(machine.Pin(red_pin), freq=980)

	while True:
		value = trimmer.read()  # Lettura trimmer
		print("Value read:", value)
		r,g,b = to_rgb(value, 1023)  # Recupero terna per PWM
		b_pin.duty(int(b))  # Impostazione pwm blu
		g_pin.duty(int(g))  # Impostazione pwm verde
		r_pin.duty(int(r))  # Impostazione pwm rosso
		time.sleep(0.001)  # Delay tra ogni lettura e aggiornamento colore
		# Delay >= 100ms, rendono distinguibili i vari step di aggiornamento
