void plotterCallback() {
    // TODO ragionare sulle scale di rappresentazione

    Serial.print(rpm());

    Serial.print(",");
    Serial.print(desiredRpm);

    /*
    Serial.print(",");
    Serial.print(potenziometro);
    */

    Serial.print(",");
    Serial.print(absDuty);

    Serial.print(",");
    Serial.print(pidResult);

    /*
    Serial.print(",");
    Serial.print(period/1000);
    */

    Serial.println();
}

void statusCallback() {
    Serial.println("---------------------------");

    Serial.print("rpm: ");
    Serial.println(rpm());

    Serial.print("desiredRpm: ");
    Serial.println(desiredRpm);

    Serial.print("absDuty: ");
    Serial.println(absDuty);

    Serial.print("lastPulse: ");
    Serial.println(lastPulse);

    Serial.print("lastTick: ");
    Serial.println(lastTick);

    Serial.print("period: ");
    Serial.println(period);

    // cursor e photovalues non ha senso per ora
}

